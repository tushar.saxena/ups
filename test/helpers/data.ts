export const shops = [
  {
    config: {
      environment: "live",
      username: "brackish",
      password: "Turkey23",
      access_key: "BCE1B7857A3A7BE6",
      imperial: true,
    },
    shipper: {
      name: "BRACKISH BOW TIES",
      shipper_number: "710V7E",
      address: {
        address_line_1: "11622 Gorham Ave",
        city: "Los Angeles",
        state_code: "CA",
        country_code: "US",
        postal_code: "90049",
      },
    },
  },
  {
    config: {
      environment: "live",
      username: "AmysBread",
      password: "Semolina1",
      access_key: "BCE1B77F8D0E0BF5",
      imperial: true,
    },
    shipper: {
      name: "Amy's Bread",
      shipper_number: "E77195",
      address: {
        address_line_1: "672 Ninth Ave",
        city: "New York",
        state_code: "NY",
        country_code: "US",
        postal_code: "10036",
      },
    },
  },
  {
    config: {
      environment: "live",
      username: "CFCS407",
      password: "Worldwide1",
      access_key: "8CFEAF4C4D92F828",
      imperial: true,
    },
    shipper: {
      name: "CENTRAL FL CABINET SUPPLY",
      shipper_number: "9EW931",
      address: {
        address_line_1: "1701 Australian Avenue",
        city: "1701 Australian Avenue",
        state_code: "FL",
        country_code: "US",
        postal_code: "33404",
      },
    },
  },
  {
    shipper: {
      name: "",
      shipper_number: "94549R",
      address: {
        address_line_1: "",
        city: "",
        state_code: "",
        country_code: "",
        postal_code: "",
      },
    },
    config: {
      environment: "live",
      username: "Beekman1802",
      password: "Amazing1802",
      access_key: "1D0973CF792B2138",
      imperial: true,
    },
  },
  {
    shipper: {
      name: "Send Joe's KC BBQ",
      shipper_number: "1A37Y1",
      address: {
        address_line_1: "11916 S. STRANG LINE RD",
        city: "OLATHE",
        state_code: "KS",
        country_code: "US",
        postal_code: "66062",
      },
    },
    config: {
      environment: "live",
      username: "sendjoe16",
      password: "Joekc16!",
      access_key: "FD0E7B905F15FF66",
      imperial: true,
    },
  },
  {
    shipper: {
      name: "",
      shipper_number: "TT2613",
      address: {
        address_line_1: "",
        city: "",
        state_code: "",
        country_code: "",
        postal_code: "",
      },
    },
    config: {
      environment: "live",
      username: "ghostly",
      password: "ghostly1",
      access_key: "ED145E360EFC1ED8",
      imperial: true,
    },
  },
  {
    shipper: {
      name: "",
      shipper_number: "T02T16",
      address: {
        address_line_1: "",
        city: "",
        state_code: "",
        country_code: "",
        postal_code: "",
      },
    },
    config: {
      environment: "live",
      username: "daviddietz.wfn",
      password: "WFNgoodf00d",
      access_key: "CD11F9844A266C18",
      imperial: true,
    },
  },
];

export const orders = [
  {
    origin: {
      type: "Unknown",
      name: "",
      street1: "1852 Wallace School Rd Ste E",
      street2: "",
      street3: "",
      city: "Charleston",
      postalCode: "29407",
      province: "SC",
      country: "US",
      countryFull: "",
      phone: "",
      company: "",
      formatted: "",
      timeZone: -1,
      isResidential: false,
    },
    destination: {
      type: "Street or Residential",
      name: "Test Test",
      street1: "33562 Sea Point Dr",
      street2: "",
      street3: "",
      city: "Dana Point",
      postalCode: "92629-1912",
      province: "CA",
      country: "US",
      countryFull: "United States of America",
      phone: "",
      company: "",
      formatted: "33562 SEA POINT DR;DANA POINT CA  92629-1912",
      timeZone: "08",
      isResidential: true,
    },
    bins: [
      {
        _id: "b5",
        name: "Pin Box (5x4x3)",
        width: 104,
        height: 79,
        depth: 130,
        max_weight: 100000,
        machinable: true,
        rectangular: true,
        weight: 224,
        price: 100,
        allowed: [
          {
            ties: 0,
            pins: 1,
          },
          {
            ties: 0,
            pins: 2,
          },
          {
            ties: 0,
            pins: 3,
          },
          {
            ties: 0,
            pins: 4,
          },
        ],
        items: [
          {
            name: "Classic - Classic",
            sku: "375265903-879964611",
            type: "Plum Thicket Pin",
            title: "Classic - Classic",
            quantity: 3,
            width: "0",
            height: "0",
            depth: "0",
            weight: 50,
            price: 4900,
            vendor: "Brackish",
            requiresShipping: true,
            taxable: true,
            fulfillmentService: "manual",
            productId: 375265903,
            variantId: 879964611,
            shopId: "Yp8LZXLVLmJmh14c",
          },
        ],
      },
    ],
    services: ["NEXT_DAY_AIR", "2ND_DAY_AIR", "GROUND"],
  },
  {
    origin: {
      type: "Unknown",
      name: "",
      street1: "672 Ninth Ave",
      street2: "",
      street3: "",
      city: "New York",
      postalCode: "10036",
      province: "NY",
      country: "US",
      countryFull: "",
      phone: "",
      company: "",
      formatted: "",
      timeZone: -1,
      isResidential: false,
    },
    destination: {
      type: "Street or Residential",
      name: "Test test",
      street1: "33562 Sea Point Dr",
      street2: "",
      street3: "",
      city: "Dana Point",
      postalCode: "92629-1912",
      province: "CA",
      country: "US",
      countryFull: "United States of America",
      phone: "5156123432",
      company: "",
      formatted: "33562 SEA POINT DR;DANA POINT CA  92629-1912",
      timeZone: "08",
      isResidential: true,
    },
    bins: [
      {
        _id: "a1",
        name: "10x10x5",
        depth: 259,
        width: 259,
        height: 131,
        max_weight: 100000,
        machinable: true,
        rectangular: true,
        weight: 1521,
        price: 0,
        items: [
          {
            name: "Apple Pecan Coffeecake",
            sku: "379561865-982819597",
            type: "Bread",
            title: "Apple Pecan Coffeecake - Default Title",
            quantity: 1,
            width: "0",
            height: "0",
            depth: "0",
            weight: 1134,
            price: 3800,
            vendor: "Amy's Bread",
            requiresShipping: true,
            taxable: false,
            fulfillmentService: "manual",
            productId: 379561865,
            variantId: 982819597,
            shopId: "hFinmELgHKbkjcdD",
          },
        ],
      },
    ],
    services: ["NEXT_DAY_AIR", "2ND_DAY_AIR", "GROUND"],
  },
  {
    origin: {
      type: "Unknown",
      name: "",
      street1: "672 Ninth Ave",
      street2: "",
      street3: "",
      city: "New York",
      postalCode: "10036",
      province: "NY",
      country: "US",
      countryFull: "",
      phone: "",
      company: "",
      formatted: "",
      timeZone: -1,
      isResidential: false,
    },
    destination: {
      type: "Street or Residential",
      name: "Test Test",
      street1: "53 Broadview Terrace",
      street2: "",
      street3: "",
      city: "Chatham",
      postalCode: "07928",
      province: "NJ",
      country: "US",
      countryFull: "United States of America",
      phone: "23422343223",
      company: "",
      formatted: "9 CHISHOLM TER;BOSTON MA  02131-4521",
      timeZone: "05",
      isResidential: true,
    },
    bins: [
      {
        _id: "a1",
        name: "10x10x5",
        depth: 259,
        width: 259,
        height: 131,
        max_weight: 100000,
        machinable: true,
        rectangular: true,
        weight: 1013,
        price: 0,
        items: [
          {
            type: "Bread",
            title: "Semolina Raisin Fennel Crisps - Default Title",
            sku: "",
            vendor: "Amy's Bread",
            width: "0",
            height: "0",
            depth: "0",
            weight: 313,
          },
        ],
      },
    ],
    services: ["NEXT_DAY_AIR", "2ND_DAY_AIR", "GROUND"],
  },
  {
    origin: {
      type: "Unknown",
      name: "",
      street1: "2617 Pemberton Drive",
      street2: "",
      street3: "",
      city: "Apopka",
      postalCode: "32703",
      province: "FL",
      country: "US",
      countryFull: "",
      phone: "",
      company: "",
      formatted: "",
      timeZone: -1,
      isResidential: false,
    },
    destination: {
      type: "Street or Residential",
      name: "Test Test",
      street1: "5633 Dorchester Road",
      street2: "",
      street3: "",
      city: "Niagara Falls",
      postalCode: "L2G 5S4",
      province: "ON",
      country: "CA",
      countryFull: "Canada",
      phone: "",
      company: "",
      timeZone: "08",
      isResidential: true,
    },
    bins: [
      {
        _id: "b5",
        name: "Pin Box (5x4x3)",
        width: 104,
        height: 79,
        depth: 130,
        max_weight: 100000,
        machinable: true,
        rectangular: true,
        weight: 224,
        price: 100,
        allowed: [
          {
            ties: 0,
            pins: 1,
          },
          {
            ties: 0,
            pins: 2,
          },
          {
            ties: 0,
            pins: 3,
          },
          {
            ties: 0,
            pins: 4,
          },
        ],
        items: [
          {
            name: "Classic - Classic",
            sku: "375265903-879964611",
            type: "Plum Thicket Pin",
            title: "Classic - Classic",
            quantity: 3,
            width: "0",
            height: "0",
            depth: "0",
            weight: 50,
            price: 4900,
            vendor: "Brackish",
            requiresShipping: true,
            taxable: true,
            fulfillmentService: "manual",
            productId: 375265903,
            variantId: 879964611,
            shopId: "Yp8LZXLVLmJmh14c",
          },
        ],
      },
    ],
    services: [
      "INTL_STANDARD",
      "INTL_WORLDWIDE_EXPEDITED",
      "INTL_WORLDWIDE_SAVER",
      "INTL_WORLDWIDE_EXPRESS",
      "INTL_WORLDWIDE_EXPRESS_PLUS",
    ],
  },
  {
    origin: {
      type: "Unknown",
      name: "",
      street1: "187 Main St",
      street2: "",
      street3: "",
      city: "Sharon Springs",
      postalCode: "13459",
      province: "NY",
      country: "US",
      countryFull: "",
      phone: "",
      company: "",
      formatted: "",
      timeZone: -1,
      isResidential: false,
    },
    destination: {
      type: "Street or Residential",
      name: "Test Test",
      street1: "Bennelong Point",
      street2: "",
      street3: "",
      city: "Sydney",
      postalCode: "2000",
      province: "NSW",
      country: "AU",
      countryFull: "Australia",
      phone: "",
      company: "",
      timeZone: "08",
      isResidential: true,
    },
    bins: [
      {
        _id: "b5",
        name: "Pin Box (5x4x3)",
        width: 104,
        height: 79,
        depth: 130,
        max_weight: 100000,
        machinable: true,
        rectangular: true,
        weight: 224,
        price: 100,
        allowed: [
          {
            ties: 0,
            pins: 1,
          },
          {
            ties: 0,
            pins: 2,
          },
          {
            ties: 0,
            pins: 3,
          },
          {
            ties: 0,
            pins: 4,
          },
        ],
        items: [
          {
            name: "Classic - Classic",
            sku: "375265903-879964611",
            type: "Plum Thicket Pin",
            title: "Classic - Classic",
            quantity: 3,
            width: "0",
            height: "0",
            depth: "0",
            weight: 50,
            price: 4900,
            vendor: "Brackish",
            requiresShipping: true,
            taxable: true,
            fulfillmentService: "manual",
            productId: 375265903,
            variantId: 879964611,
            shopId: "Yp8LZXLVLmJmh14c",
          },
        ],
      },
    ],
    services: [
      "INTL_STANDARD",
      "INTL_WORLDWIDE_EXPEDITED",
      "INTL_WORLDWIDE_SAVER",
      "INTL_WORLDWIDE_EXPRESS",
      "INTL_WORLDWIDE_EXPRESS_PLUS",
    ],
  },
  {
    origin: {
      type: "Unknown",
      name: "",
      street1: "11916 South Strang Line Road",
      street2: "",
      street3: "",
      city: "Olathe",
      postalCode: "66062",
      province: "KS",
      country: "US",
      countryFull: "",
      phone: "7323052870",
      company: "SendJoe'sKCBBQ",
      formatted: "",
      timeZone: null,
      isResidential: false,
    },
    destination: {
      type: "Unknown",
      name: "Test Test",
      street1: "10900 WILSHIRE BLVD",
      street2: "FL 17",
      street3: "",
      city: "LOS ANGELES",
      postalCode: "90024",
      province: "CA",
      country: "US",
      countryFull: "United States of America",
      phone: "+16167061234",
      company: "",
      formatted: "10900 WILSHIRE BLVD;LOS ANGELES CA  90024",
      timeZone: "06",
      isResidential: true,
    },
    bins: [
      {
        id: "j2",
        name: "Small Box",
        thickness: 1,
        depth: 453.9,
        width: 355.6,
        height: 330.2,
        max_weight: 100000,
        machinable: true,
        rectangular: true,
        type: "box",
        weight: 15081.65,
        price: 0,
        items: [
          {
            name: "Z-Man Kit - Chicken",
            parent_id: "",
            sku: "3431",
            type: "Food",
            title: "Z-Man Kit - Chicken - Default Title",
            quantity: 2,
            dimensions: [],
            weight: 3175,
            price: 8499,
            vendor: "SendJoesKCBBQ",
            requiresShipping: true,
            taxable: true,
            fulfillmentService: "manual",
            productId: 1666799169,
            variantId: 4982180033,
            storeId: "5761794737650c1d2450b5cd",
            meta: {},
            tags: ["75-100", "Poultry", "serves-8-10", "Z-Man"],
            customProperties: {},
            inventory: -2,
            units: 0,
          },
        ],
        units: 6,
      },
    ],
    services: ["NEXT_DAY_AIR", "2ND_DAY_AIR", "GROUND"],
  },
  {
    origin: {
      type: "Unknown",
      name: "",
      street1: "35 Alston Drive",
      street2: "",
      street3: "",
      city: "Milton Keynes",
      postalCode: "MK13 9HA",
      province: "",
      country: "UK",
      countryFull: "",
      phone: "",
      company: "",
      formatted: "",
      timeZone: null,
      isResidential: false,
    },
    destination: {
      type: "Unknown",
      name: "Test Test",
      street1: "32333422 Knellwood",
      street2: "",
      street3: "",
      city: "Taucha",
      postalCode: "04425",
      province: "",
      country: "DE",
      countryFull: "Germany",
      phone: "",
      company: "",
      formatted: "32333422 Knellwood;GERMANY",
      timeZone: "0 ",
      isResidential: true,
    },
    bins: [
      {
        id: "g1",
        name: "Unknown",
        thickness: 4,
        depth: 100,
        width: 100,
        height: 100,
        max_weight: 100000,
        machinable: true,
        rectangular: true,
        type: "box",
        weight: 100,
        price: 0,
        items: [
          {
            name:
              "10 Principles for Good Design Poster - Poster: US S+H Included",
            parent_id: "",
            sku: "300000001200",
            type: "Goods",
            title:
              "10 Principles for Good Design Poster - Poster: US S+H Included",
            quantity: 1,
            dimensions: [],
            weight: 0,
            price: 4000,
            vendor: "Vitsœ | Dieter Rams",
            requiresShipping: true,
            taxable: true,
            fulfillmentService: "manual",
            productId: 7251466049,
            variantId: 22854746305,
            storeId: "57c55bc42d6c2e2d7e2020ab",
            meta: {},
            tags: ["Europe"],
            customProperties: {},
            inventory: 21,
          },
        ],
        warehouse: "UK",
      },
    ],
    services: [
      "NEXT_DAY_AIR",
      "NEXT_DAY_AIR_SAVER",
      "2ND_DAY_AIR",
      "3_DAY_SELECT",
      "GROUND",
      "INTL_WORLDWIDE_SAVER",
    ],
  },
];

export default {
  shops,
  orders,
};
