import moment from "moment";

import UPSCarrier from "../src/lib/ups";

const data = {
  destination: {
    type: "Unknown",
    name: " ",
    city: "sydney",
    province: "NSW",
    country: "AU",
    phone: "",
    company: "",
    street1: "2324 MAin",
    street2: "",
    street3: "",
    postalCode: "2002",
    isResidential: null,
  },
  items: [
    {
      id: "4QH1NP-yJFpg-hRB5H7Ov39593472917601",
      product_id: "4QH1NP-yJFpg-hRB5H7Ov",
      variant_id: 39593472917601,
      name:
        "20-piece Gift Box with Custom Sprinkles + Standard Flavors(Peppermint / Coconut) - 0.8 lb",
      product_name:
        "20-piece Gift Box with Custom Sprinkles + Standard Flavors",
      variant_name: "Peppermint / Coconut",
      quantity: 1,
      price: 3195,
      imageUrl:
        "https://cdn.shopify.com/s/files/1/2385/5869/products/Screen_Shot_2019-05-10_at_12.01.02_PM_73001c87-302d-4d74-8a61-11258a98ce8d.png?v=1634675376",
      weight: 0.8,
      weight_unit: "lb",
      tags: "FS Team",
      grams: 340.19399999999996,
    },
  ],
  bins: [
    {
      name: "Default Box",
      weight: 340.19399999999996,
      items: [
        {
          id: "4QH1NP-yJFpg-hRB5H7Ov39593472917601",
          product_id: "4QH1NP-yJFpg-hRB5H7Ov",
          variant_id: 39593472917601,
          name:
            "20-piece Gift Box with Custom Sprinkles + Standard Flavors(Peppermint / Coconut) - 0.8 lb",
          product_name:
            "20-piece Gift Box with Custom Sprinkles + Standard Flavors",
          variant_name: "Peppermint / Coconut",
          quantity: 1,
          price: 3195,
          imageUrl:
            "https://cdn.shopify.com/s/files/1/2385/5869/products/Screen_Shot_2019-05-10_at_12.01.02_PM_73001c87-302d-4d74-8a61-11258a98ce8d.png?v=1634675376",
          weight: 0.8,
          weight_unit: "lb",
          tags: "FS Team",
          grams: 340.19399999999996,
        },
      ],
      height: 100,
      width: 100,
      depth: 100,
    },
  ],
  shipDate: moment().format(),
  origin: {
    type: "Unknown",
    name: "",
    city: "Springfield",
    province: "NY",
    country: "US",
    phone: "",
    company: "",
    street1: "630 Flushing Avenue Brooklyn",
    street2: "",
    street3: "",
    postalCode: "11206",
  },
  ups_config: {
    shipper: {
      name: "",
      shipper_number: "5345F6",
      address: {
        address_line_1: "",
        city: "",
        state_code: "",
        country_code: "",
        postal_code: "",
      },
    },
    config: {
      environment: "live",
      username: "FattySundays",
      password: "[***]",
      access_key: "9D4DC3E9FD8F091D",
      saturday: true,
      imperial: true,
    },
  },
  signature_required: false,
};

describe("UPS", () => {
  describe("FTS", () => {
    test("canada", async () => {
      const ups = new UPSCarrier(
        data.ups_config.config,
        data.ups_config.shipper,
      );

      const rates = await ups.getRates(
        data.origin,
        data.destination,
        data.bins,
        data.shipDate,
        [
          "INTL_WORLDWIDE_SAVER"
        ],
      );

      console.log(rates);
    }, 10000);
  });
});
