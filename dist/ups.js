import UPSAPI from "@decimal/ups-sdk";
import _ from "lodash";
import moment from "moment";
import { promisify } from "util";
const states = [
    "AL",
    "Alabama",
    "AK",
    "Alaska",
    "Hawaii",
    "HI",
    "DC",
    "AZ",
    "Arizona",
    "AR",
    "Arkansas",
    "CA",
    "California",
    "CO",
    "Colorado",
    "CT",
    "Connecticut",
    "DE",
    "Delaware",
    "DC",
    "FL",
    "Florida",
    "GA",
    "Georgia",
    "ID",
    "Idaho",
    "IL",
    "Illinois",
    "IN",
    "Indiana",
    "IA",
    "Iowa",
    "KS",
    "Kansas",
    "KY",
    "Kentucky",
    "LA",
    "Louisiana",
    "ME",
    "Maine",
    "MD",
    "Maryland",
    "MA",
    "Massachusetts",
    "MI",
    "Michigan",
    "MN",
    "Minnesota",
    "MS",
    "Mississippi",
    "MO",
    "Missouri",
    "MT",
    "Montana",
    "NE",
    "Nebraska",
    "NV",
    "Nevada",
    "NH",
    "New Hampshire",
    "NJ",
    "New Jersey",
    "NM",
    "New Mexico",
    "NY",
    "New York",
    "NC",
    "North Carolina",
    "ND",
    "North Dakota",
    "OH",
    "Ohio",
    "OK",
    "Oklahoma",
    "OR",
    "Oregon",
    "PA",
    "Pennsylvania",
    "RI",
    "Rhode Island",
    "SC",
    "South Carolina",
    "SD",
    "South Dakota",
    "TN",
    "Tennessee",
    "TX",
    "Texas",
    "UT",
    "Utah",
    "VT",
    "Vermont",
    "VA",
    "Virginia",
    "WA",
    "Washington",
    "WV",
    "West Virginia",
    "WI",
    "Wisconsin",
    "WY",
    "Wyoming",
];
export function isClosedDate(d, holidays) {
    if (_.includes([0, 6], d.day())) {
        return true;
    }
    const ymd = d.format("YYYY-MM-DD");
    return _.find(holidays, (cl) => cl === ymd);
}
export function getNextUnclosed(d, holidays, min = 1) {
    let next = d.clone();
    while (min > 0) {
        if (isClosedDate(next, holidays)) {
            do {
                next = next.add(1, "days");
            } while (isClosedDate(next, holidays));
        }
        else {
            next = next.add(1, "days");
            min--;
        }
    }
    return next;
}
export default class UPS {
    constructor(config, shipper) {
        this.config = config;
        this.shipper = shipper;
    }
    async getRates(shipper, recipient, orderBins, shipDate, services) {
        const { config } = this;
        if (shipper.country !== "US" && shipper.country !== "United States") {
            config.imperial = false;
        }
        if ((recipient.country === "US" || recipient.country === "United States")
            && !_.includes(states, recipient.province)) {
            recipient.country = recipient.province;
        }
        config.negotiated_rates = true;
        const ups = new UPSAPI(config);
        const bins = [];
        let weight = 0;
        let value = 0;
        const num = orderBins.length;
        _.forEach(orderBins, (bin) => {
            const upsBin = _.clone(bin);
            if (upsBin.weight < 100) {
                upsBin.weight = 100;
            }
            if (config.imperial) {
                upsBin.weight = Math.round(upsBin.weight * 0.00220462 * 10) / 10;
                upsBin.dimensions = {
                    length: Math.round(upsBin.depth * 0.0393701 * 10) / 10,
                    width: Math.round(upsBin.width * 0.0393701 * 10) / 10,
                    height: Math.round(upsBin.height * 0.0393701 * 10) / 10,
                };
            }
            weight += upsBin.weight;
            value += bin.price;
            for (const item of bin.items) {
                value += item.price;
            }
            bins.push(upsBin);
        });
        let surepostCode = "93";
        if ((config.imperial && weight < 1)
            || (!config.imperial && weight < 453.59237)) {
            surepostCode = "92";
        }
        if (recipient.type === "PO Box") {
            return [];
        }
        if (_.includes(["AA", "AE", "AP"], recipient.province)) {
            return [];
        }
        if (value < 100) {
            value = 100;
        }
        if (value > 10000) {
            value = 10000;
        }
        const transitRequest = {
            to: {
                city: recipient.city,
                state_code: recipient.province,
                country_code: recipient.country,
                postal_code: recipient.postalCode,
                residential: recipient.isResidential,
            },
            from: {
                city: shipper.city,
                state_code: shipper.province,
                country_code: shipper.country,
                postal_code: shipper.postalCode,
            },
            pickup_date: moment(shipDate).format("YYYYMMDD"),
            weight,
            total_packages: num,
            value: value / 100,
        };
        if (config.saturday) {
            transitRequest.saturday_delivery = true;
        }
        const getTransitTimes = promisify(ups.time_in_transit).bind(ups);
        const transitTimes = await getTransitTimes(transitRequest);
        console.log(JSON.stringify(transitTimes));
        if (transitTimes.Response.ResponseStatusCode !== "1") {
            throw new Error(transitTimes.Response.ResponseStatusDescription);
        }
        const rateRequest = {
            pickup_type: "daily_pickup",
            shipper: this.shipper,
            ship_to: {
                company_name: recipient.name,
                address: {
                    address_line_1: recipient.street1,
                    city: recipient.city,
                    state_code: recipient.province,
                    country_code: recipient.country,
                    postal_code: recipient.postalCode,
                    residential: recipient.isResidential,
                },
            },
            ship_from: {
                name: this.shipper.name,
                address: {
                    address_line_1: shipper.street1,
                    city: shipper.city,
                    state_code: shipper.province,
                    country_code: shipper.country,
                    postal_code: shipper.postalCode,
                },
            },
            services: [],
            packages: _.map(bins, (bin) => ({
                weight: bin.weight,
                dimensions: bin.dimensions,
            })),
            saturday_delivery: config.saturday,
        };
        if (config.saturday) {
            rateRequest.saturday_delivery = true;
        }
        _.forEach(services, (service) => {
            if ((recipient.country === "US"
                || recipient.country === "United States")
                && (shipper.country === "US" || shipper.country === "United States")) {
                switch (service) {
                    case "NEXT_DAY_AIR":
                        rateRequest.services.push("01");
                        break;
                    case "NEXT_DAY_AIR_AM":
                        rateRequest.services.push("14");
                        break;
                    case "NEXT_DAY_AIR_SAVER":
                        rateRequest.services.push("13");
                        break;
                    case "2ND_DAY_AIR":
                        rateRequest.services.push("02");
                        break;
                    case "2ND_DAY_AIR_AM":
                        rateRequest.services.push("59");
                        break;
                    case "3_DAY_SELECT":
                        rateRequest.services.push("12");
                        break;
                    case "SUREPOST":
                        rateRequest.services.push(surepostCode);
                        rateRequest.packages[0].weight = Math.ceil(rateRequest.packages[0].weight * 16);
                        rateRequest.packages[0].dimensions.height = Math.ceil(rateRequest.packages[0].dimensions.height);
                        rateRequest.packages[0].dimensions.width = Math.ceil(rateRequest.packages[0].dimensions.width);
                        rateRequest.packages[0].dimensions.length = Math.ceil(rateRequest.packages[0].dimensions.length);
                        break;
                    case "GROUND":
                        rateRequest.services.push("03");
                        break;
                    default:
                }
            }
            else {
                if ((shipper.country === "US" || shipper.country === "United States")
                    && (recipient.country === "CA"
                        || recipient.country === "Canada"
                        || recipient.country === "MX"
                        || recipient.country === "Mexico")
                    && service === "INTL_STANDARD") {
                    rateRequest.services.push("11");
                }
                switch (service) {
                    case "INTL_WORLDWIDE_EXPRESS":
                        rateRequest.services.push("07");
                        break;
                    case "INTL_WORLDWIDE_EXPRESS_PLUS":
                        rateRequest.services.push("54");
                        break;
                    case "INTL_WORLDWIDE_EXPEDITED":
                        rateRequest.services.push("08");
                        break;
                    case "INTL_WORLDWIDE_SAVER":
                        rateRequest.services.push("65");
                        break;
                    default:
                }
            }
        });
        const processedRates = {
            raw: {
                transit: transitTimes,
            },
        };
        const momentShipDate = moment(shipDate);
        _.forEach(transitTimes.TransitResponse.ServiceSummary, (service) => {
            const momentDeliveryDate = moment(`${service.EstimatedArrival.Date} ${service.EstimatedArrival.Time}`);
            if (_.includes(services, "NEXT_DAY_AIR")
                && service.Service.Code === "1DA") {
                processedRates.NEXT_DAY_AIR = {
                    serviceType: "NEXT_DAY_AIR",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "NEXT_DAY_AIR_AM")
                && service.Service.Code === "1DM") {
                processedRates.NEXT_DAY_AIR_AM = {
                    serviceType: "NEXT_DAY_AIR_AM",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "NEXT_DAY_AIR_SAVER")
                && service.Service.Code === "1DP") {
                processedRates.NEXT_DAY_AIR_SAVER = {
                    serviceType: "NEXT_DAY_AIR_SAVER",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "2ND_DAY_AIR")
                && service.Service.Code === "2DA") {
                processedRates.T2ND_DAY_AIR = {
                    serviceType: "2ND_DAY_AIR",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "2ND_DAY_AIR_AM")
                && service.Service.Code === "2DM") {
                processedRates.T2ND_DAY_AIR_AM = {
                    serviceType: "2ND_DAY_AIR_AM",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "3_DAY_SELECT")
                && service.Service.Code === "3DS") {
                processedRates.T3_DAY_SELECT = {
                    serviceType: "3_DAY_SELECT",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "GROUND")
                && service.Service.Code === "GND") {
                processedRates.GROUND = {
                    serviceType: "GROUND",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "INTL_STANDARD")
                && service.Service.Code === "03") {
                processedRates.INTL_STANDARD = {
                    serviceType: "INTL_STANDARD",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "INTL_WORLDWIDE_EXPRESS")
                && service.Service.Code === "01") {
                processedRates.INTL_WORLDWIDE_EXPRESS = {
                    serviceType: "INTL_WORLDWIDE_EXPRESS",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "INTL_WORLDWIDE_EXPRESS_PLUS")
                && service.Service.Code === "21") {
                processedRates.INTL_WORLDWIDE_EXPRESS_PLUS = {
                    serviceType: "INTL_WORLDWIDE_EXPRESS_PLUS",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "INTL_WORLDWIDE_EXPEDITED")
                && service.Service.Code === "05") {
                processedRates.INTL_WORLDWIDE_EXPEDITED = {
                    serviceType: "INTL_WORLDWIDE_EXPEDITED",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
            else if (_.includes(services, "INTL_WORLDWIDE_SAVER")
                && service.Service.Code === "28") {
                processedRates.INTL_WORLDWIDE_SAVER = {
                    serviceType: "INTL_WORLDWIDE_SAVER",
                    time: momentDeliveryDate.format("hh:mm A"),
                    date: momentDeliveryDate.format("YYYY-MM-DD"),
                    transit: moment(service.EstimatedArrival.Date).diff(shipDate, "days")
                        + 1,
                    shipdate: momentShipDate.format("YYYY-MM-DD"),
                };
            }
        });
        if (!processedRates.T2ND_DAY_AIR_AM) {
            let arrival = momentShipDate.clone().add(2, "days");
            arrival = getNextUnclosed(arrival, this.config.holidays);
            processedRates.T2ND_DAY_AIR_AM = {
                serviceType: "2ND_DAY_AIR_AM",
                time: "12:00 PM",
                date: arrival.format("YYYY-MM-DD"),
                transit: arrival.diff(momentShipDate, "days"),
                shipdate: momentShipDate.format("YYYY-MM-DD"),
            };
        }
        if (!processedRates.T2ND_DAY_AIR) {
            let arrival = momentShipDate.clone();
            arrival = getNextUnclosed(arrival, this.config.holidays, 2);
            processedRates.T2ND_DAY_AIR = {
                serviceType: "2ND_DAY_AIR",
                time: "11:00 PM",
                date: arrival.format("YYYY-MM-DD"),
                transit: arrival.diff(momentShipDate, "days"),
                shipdate: momentShipDate.format("YYYY-MM-DD"),
            };
        }
        if (!processedRates.T3_DAY_SELECT) {
            let arrival = momentShipDate.clone();
            arrival = getNextUnclosed(arrival, this.config.holidays, 3);
            processedRates.T3_DAY_SELECT = {
                serviceType: "3_DAY_SELECT",
                time: "11:00 PM",
                date: arrival.format("YYYY-MM-DD"),
                transit: arrival.diff(momentShipDate, "days"),
                shipdate: momentShipDate.format("YYYY-MM-DD"),
            };
        }
        const getRates = promisify(ups.rates).bind(ups);
        const rates = await getRates(rateRequest);
        if (rates.Response.ResponseStatusCode !== "1") {
            throw new Error(transitTimes.Response.ResponseStatusDescription);
        }
        processedRates.raw.rates = rates;
        _.forEach(rates.RatedShipment, (rate) => {
            let carrierPrice;
            if (rate.NegotiatedRates) {
                carrierPrice = Math.round(Number(rate.NegotiatedRates.NetSummaryCharges.GrandTotal.MonetaryValue) * 100);
            }
            else {
                carrierPrice = Math.round(Number(rate.TotalCharges.MonetaryValue) * 100);
            }
            if (_.includes(services, "NEXT_DAY_AIR")
                && (rate.Service.Code === 1 || rate.Service.Code === "01")) {
                if (processedRates.NEXT_DAY_AIR) {
                    processedRates.NEXT_DAY_AIR.price = carrierPrice;
                }
            }
            else if (_.includes(services, "NEXT_DAY_AIR_AM")
                && (rate.Service.Code === 14 || rate.Service.Code === "14")) {
                if (processedRates.NEXT_DAY_AIR_AM) {
                    processedRates.NEXT_DAY_AIR_AM.price = carrierPrice;
                }
            }
            else if (_.includes(services, "NEXT_DAY_AIR_SAVER")
                && (rate.Service.Code === 13 || rate.Service.Code === "13")) {
                if (processedRates.NEXT_DAY_AIR_SAVER) {
                    processedRates.NEXT_DAY_AIR_SAVER.price = carrierPrice;
                }
            }
            else if (_.includes(services, "2ND_DAY_AIR")
                && (rate.Service.Code === 2 || rate.Service.Code === "02")) {
                if (processedRates.T2ND_DAY_AIR) {
                    processedRates.T2ND_DAY_AIR.price = carrierPrice;
                }
            }
            else if (_.includes(services, "2ND_DAY_AIR_AM")
                && (rate.Service.Code === 59 || rate.Service.Code === "59")) {
                if (processedRates.T2ND_DAY_AIR_AM) {
                    processedRates.T2ND_DAY_AIR_AM.price = carrierPrice;
                }
            }
            else if (_.includes(services, "3_DAY_SELECT")
                && (rate.Service.Code === 12 || rate.Service.Code === "12")) {
                if (processedRates.T3_DAY_SELECT) {
                    processedRates.T3_DAY_SELECT.price = carrierPrice;
                }
            }
            else if (_.includes(services, "GROUND")
                && (rate.Service.Code === 3 || rate.Service.Code === "03")) {
                if (processedRates.GROUND) {
                    processedRates.GROUND.price = carrierPrice;
                }
            }
            else if (_.includes(services, "INTL_STANDARD")
                && (rate.Service.Code === 11 || rate.Service.Code === "11")) {
                if (processedRates.INTL_STANDARD) {
                    processedRates.INTL_STANDARD.price = carrierPrice;
                }
            }
            else if (_.includes(services, "INTL_WORLDWIDE_EXPRESS")
                && (rate.Service.Code === 7 || rate.Service.Code === "07")) {
                if (processedRates.INTL_WORLDWIDE_EXPRESS) {
                    processedRates.INTL_WORLDWIDE_EXPRESS.price = carrierPrice;
                }
            }
            else if (_.includes(services, "INTL_WORLDWIDE_EXPRESS_PLUS")
                && (rate.Service.Code === 54 || rate.Service.Code === "54")) {
                if (processedRates.INTL_WORLDWIDE_EXPRESS_PLUS) {
                    processedRates.INTL_WORLDWIDE_EXPRESS_PLUS.price = carrierPrice;
                }
            }
            else if (_.includes(services, "INTL_WORLDWIDE_EXPEDITED")
                && (rate.Service.Code === 8 || rate.Service.Code === "08")) {
                if (processedRates.INTL_WORLDWIDE_EXPEDITED) {
                    processedRates.INTL_WORLDWIDE_EXPEDITED.price = carrierPrice;
                }
            }
            else if (_.includes(services, "INTL_WORLDWIDE_SAVER")
                && (rate.Service.Code === 65 || rate.Service.Code === "65")) {
                if (processedRates.INTL_WORLDWIDE_SAVER) {
                    processedRates.INTL_WORLDWIDE_SAVER.price = carrierPrice;
                }
            }
        });
        const finalRates = [];
        if (processedRates.NEXT_DAY_AIR && processedRates.NEXT_DAY_AIR.price) {
            finalRates.push(processedRates.NEXT_DAY_AIR);
        }
        if (processedRates.NEXT_DAY_AIR_AM
            && processedRates.NEXT_DAY_AIR_AM.price) {
            finalRates.push(processedRates.NEXT_DAY_AIR_AM);
        }
        if (processedRates.NEXT_DAY_AIR_SAVER
            && processedRates.NEXT_DAY_AIR_SAVER.price) {
            finalRates.push(processedRates.NEXT_DAY_AIR_SAVER);
        }
        if (processedRates.T2ND_DAY_AIR && processedRates.T2ND_DAY_AIR.price) {
            finalRates.push(processedRates.T2ND_DAY_AIR);
        }
        if (processedRates.T2ND_DAY_AIR_AM
            && processedRates.T2ND_DAY_AIR_AM.price) {
            finalRates.push(processedRates.T2ND_DAY_AIR_AM);
        }
        if (processedRates.T3_DAY_SELECT
            && processedRates.T3_DAY_SELECT.price) {
            finalRates.push(processedRates.T3_DAY_SELECT);
        }
        if (processedRates.GROUND && processedRates.GROUND.price) {
            finalRates.push(processedRates.GROUND);
        }
        if (processedRates.INTL_STANDARD
            && processedRates.INTL_STANDARD.price) {
            finalRates.push(processedRates.INTL_STANDARD);
        }
        if (processedRates.INTL_WORLDWIDE_EXPRESS
            && processedRates.INTL_WORLDWIDE_EXPRESS.price) {
            finalRates.push(processedRates.INTL_WORLDWIDE_EXPRESS);
        }
        if (processedRates.INTL_WORLDWIDE_EXPRESS_PLUS
            && processedRates.INTL_WORLDWIDE_EXPRESS_PLUS.price) {
            finalRates.push(processedRates.INTL_WORLDWIDE_EXPRESS_PLUS);
        }
        if (processedRates.INTL_WORLDWIDE_EXPEDITED
            && processedRates.INTL_WORLDWIDE_EXPEDITED.price) {
            finalRates.push(processedRates.INTL_WORLDWIDE_EXPEDITED);
        }
        if (processedRates.INTL_WORLDWIDE_SAVER
            && processedRates.INTL_WORLDWIDE_SAVER.price) {
            finalRates.push(processedRates.INTL_WORLDWIDE_SAVER);
        }
        return finalRates;
    }
}
//# sourceMappingURL=ups.js.map