import moment, { Moment } from "moment";
export declare function isClosedDate(d: Moment, holidays: string[]): string | true;
export declare function getNextUnclosed(d: Moment, holidays: string[], min?: number): moment.Moment;
export default class UPS {
    private readonly config;
    private readonly shipper;
    constructor(config: any, shipper: any);
    getRates(shipper: any, recipient: any, orderBins: any[], shipDate: string, services: string[]): Promise<any[]>;
}
